package fr.ulille.iut.m4102;

/**
 * Cette classe introduit un intermédiaire entre la classe utilisatrice
 * et l'implémentation du traitement des chaînes.
 * Au début cette classe se contente de logger ce qui se passe puis
 * elle va évoluer pour accéder au service à distance.
 * Le comportement de cette classe est totalement transparent pour la
 * classe Utilisatrice qui au final utilise les mêmes méthodes que si elle
 * appelait directement la classe AlaChaine.
 */
public class Intermediaire implements AlaChaineInterface {
    private AlaChaine alc;
    
    public Intermediaire() {
	// pour l'instant on accède directement au service après instanciation
	alc = new AlaChaine();
    }
    
    public int nombreMots(String chaine) {
	return alc.nombreMots(chaine);
    }

    public String asphyxie(String chaine) throws PasDAirException {
	return alc.asphyxie(chaine);
    }

    public String leetSpeak(String chaine) {
	return alc.leetSpeak(chaine);
    }

    public int compteChar(String chaine, char c) {
	return alc.compteChar(chaine, c);
    }

}
